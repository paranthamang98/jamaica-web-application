var uploaderBtn = document.querySelector(".input");
var uploadername = document.querySelector(".filepath");
var user = document.querySelector(".user_icon");
var userContent = document.querySelector(".user_icon_popup");
var adds = document.querySelector(".adds_close");
var adds_close = document.querySelector(".mobile_add");
var edit_section = document.querySelector(".profile_section");
var edit_section_btn = document.querySelector(".edit_button");
var edit_section_btn_close = document.querySelector(".update_cancal");
var edit_section_btn_update = document.querySelector(".update_data");



// menu toggule
$(document).ready(function () {
  $(".menu_icon").click(function () {
    $(".navlist").toggleClass("open");
    userContent.classList.remove("shadow");
    $(this)
      .toggleClass("opened")
      .setAttribute("aria-expanded", this.classList.contains("opened"));
  });
});

//chat_bot_icon_section

$(".chat_bot_icon_section").click(function (){
  $(".chat_bot").toggleClass("active");
})

// decrease increase
$(function () {
  $("[data-decrease]").click(decrease);
  $("[data-increase]").click(increase);
  $("[data-value]").change(valueChange);
});

function decrease() {
  var value = $(this).parent().find("[data-value]").val();
  if (value > 1) {
    value--;
    $(this).parent().find("[data-value]").val(value);
    $("[data-decrease]").removeAttr("disabled");
  } else if (value <= 1) {
    $("[data-decrease]").attr("disabled", "disabled");
  }
}

function increase() {
  var value = $(this).parent().find("[data-value]").val();
  if (value < 100) {
    value++;
    $(this).parent().find("[data-value]").val(value);
    $("[data-decrease]").removeAttr("disabled");
  }
}

function valueChange() {
  var value = $(this).val();
  if (value == undefined || isNaN(value) == true || value <= 0) {
    $(this).val(1);
  } else if (value >= 101) {
    $(this).val(100);
  }
}

// adds section
adds.addEventListener("click", () => {
  adds_close.classList.add("hide");
});

setTimeout(function () {
  $(".adds_close").addClass("highlight");
}, 6000);

// location

$(document).ready(function () {
  $(".location").click(function () {
    $(".searchLocation").slideDown();
  });
  $(".locationInput").click(function () {
    $(".location_list").slideDown();
  });

  $(".location_icon").click(function () {
    $(".searchLocation").slideUp();
    $(".getLocation").slideDown();
  });
});

// upload profile

const editFunction = (seletor, values) => {
  seletor.addEventListener("click", () => {
    values
      ? edit_section.classList.add("edit")
      : edit_section.classList.remove("edit");
  });
};
editFunction(edit_section_btn, true);
editFunction(edit_section_btn_close, false);
editFunction(edit_section_btn_update, false);
// upload profile end

// img uploader
uploaderBtn.addEventListener("change", function (event) {
  if (event.target.files.length > 0) {
    var src = URL.createObjectURL(event.target.files[0]);
    var preview = (document.querySelector(".img").src = src);
    uploadername.innerHTML = event.target.files[0].name;
  }
});
// img uploader



